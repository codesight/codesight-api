FROM openjdk:11-jdk

WORKDIR /srv/app

COPY target/*.jar app.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/srv/app/app.jar"]
