package com.codesight.codesight_api.domain.user.service;

import com.codesight.codesight_api.domain.user.entity.ApplicationUserRole;
import com.codesight.codesight_api.domain.user.entity.User;
import com.codesight.codesight_api.domain.user.repository.UserRepository;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.shared.IncorrectJsonMergePatchProcessingException;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.shared.IdCannotBeChangedException;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.users.UserAlreadyExistsException;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.users.UserNotFoundException;
import com.codesight.codesight_api.web.dtos.user.UserGetDto;
import com.codesight.codesight_api.web.dtos.user.UserPostDto;
import com.codesight.codesight_api.web.mappers.UserMapperImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatchException;
import com.github.fge.jsonpatch.mergepatch.JsonMergePatch;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Spy
    private UserMapperImpl userMapper;

    @Spy
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Spy
    private ObjectMapper objectMapper;

    @InjectMocks
    private UserServiceImpl userService;

    private static User user;
    private static UserPostDto userPostDto;

    @BeforeAll
    public static void initBeforeAll(){
        user = new User(1, "John", "john88@gmail.com", "Jhonny88!", ApplicationUserRole.ADMIN);
        userPostDto = new UserPostDto("John", "john88@gmail.com", "Jhonny88!", ApplicationUserRole.ADMIN);
    }

    @Test
    void getAllShouldReturnAllUsers(){
        List<User> predefinedList = new ArrayList<>();

        User user2 = new User(2, "Mirjana Bulat", "mbulat71@@gmail.com", "Mirjana27!", ApplicationUserRole.ADMIN);
        User user3 = new User(3, "Zdravko Bulat", "zbulat73@gmail.com", "Zdravko27!", ApplicationUserRole.COMPETITOR);

        predefinedList.add(user);
        predefinedList.add(user2);
        predefinedList.add(user3);

        Pageable pageable = PageRequest.of(0,3);
        final Page<User> predefinedPage = new PageImpl<>(predefinedList);

        when(userRepository.findAll(pageable)).thenReturn(predefinedPage);

        Page<UserGetDto> fetchedPage = userService.get(pageable);

        assertThat(fetchedPage).usingRecursiveComparison().isEqualTo(predefinedPage);
    }

    @Test
    void getByIdShouldReturnUserById(){
        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));

        UserGetDto fetchedUser = userService.get(user.getId());

        assertThat(fetchedUser).usingRecursiveComparison().isEqualTo(user);
    }

    @Test
    void willThrowWhenUserDoesntExistGetById(){
        when(userRepository.findById(anyInt())).thenReturn(Optional.empty());

        Assertions.assertThrows(UserNotFoundException.class, () ->{
            userService.get(user.getId());
                });
    }

    @Test
    void createUserShouldAddUser(){
        userService.create(userPostDto);

        verify(userRepository, times(1)).save(any(User.class));
    }

    @Test
    void willThrowWhenUserAlreadyExistsCreate(){
        when(userRepository.existsByEmail(anyString())).thenReturn(true);

        Assertions.assertThrows(UserAlreadyExistsException.class, () -> {
           userService.create(userPostDto);
        });
    }

    @Test
    void deleteByIdShouldDeleteUser(){
        when(userRepository.existsById(anyInt())).thenReturn(true);

        userService.delete(user.getId());

        verify(userRepository, times(1)).deleteById(user.getId());
    }

    @Test
    void willThrowWhenUserDoesntExistDeleteById(){
        when(userRepository.existsById(anyInt())).thenReturn(false);

        Assertions.assertThrows(UserNotFoundException.class, ()->{
            userService.delete(user.getId());
        });
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForUserPatch")
    void partialUpdateByIdShouldUpdateUser(String jsonString, User userAfterUpdate) throws JsonProcessingException, JsonPatchException {
        userAfterUpdate.setPassword(bCryptPasswordEncoder.encode(userAfterUpdate.getPassword()));
        JsonNode jsonNode = objectMapper.readTree(jsonString);
        JsonMergePatch patch = JsonMergePatch.fromJson(jsonNode);

        when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));
        when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.ofNullable(user));

        userService.partialUpdate(user.getId(), patch, user.getEmail());

        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);

        verify(userRepository).save(userArgumentCaptor.capture());

        User capturedUser = userArgumentCaptor.getValue();

        assertThat(capturedUser.getEmail()).isEqualTo(userAfterUpdate.getEmail());
        assertThat(capturedUser.getName()).isEqualTo(userAfterUpdate.getName());
        assertThat(capturedUser.getRole()).isEqualTo(userAfterUpdate.getRole());
    }

    @Test
    void willThrowWhenTryingToChangeId() throws JsonProcessingException, JsonPatchException {
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.ofNullable(user));
        String jsonString = "{\"id\": \"5\"}";
        JsonNode jsonNode = objectMapper.readTree(jsonString);
        JsonMergePatch patch = JsonMergePatch.fromJson(jsonNode);

        Assertions.assertThrows(IdCannotBeChangedException.class, ()->{
           userService.partialUpdate(user.getId(), patch, user.getEmail());
        });
    }

    @Test
    void willThrowWhenUserAlreadyExistsPartialUpdate() throws JsonProcessingException, JsonPatchException {
        when(userRepository.existsByEmail("bulatn@gmail.com")).thenReturn(true);
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.ofNullable(user));
        String jsonString = "{\"email\": \"bulatn@gmail.com\"}";
        JsonNode jsonNode = objectMapper.readTree(jsonString);
        JsonMergePatch patch = JsonMergePatch.fromJson(jsonNode);

        Assertions.assertThrows(UserAlreadyExistsException.class, ()->{
            userService.partialUpdate(user.getId(), patch, user.getEmail());
        });
    }

    @Test
    void willThrowWhenJsonFormatIsNotRight() throws JsonProcessingException, JsonPatchException {
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.ofNullable(user));
        String jsonString = "{\"role\": \"Random\"}";
        JsonNode jsonNode = objectMapper.readTree(jsonString);
        JsonMergePatch patch = JsonMergePatch.fromJson(jsonNode);

        Assertions.assertThrows(IncorrectJsonMergePatchProcessingException.class, ()->{
            userService.partialUpdate(user.getId(), patch, user.getEmail());
        });
    }

    private static List<Arguments> provideArgumentsForUserPatch(){
        return List.of(
                Arguments.of("{\"name\" : \"Jovan\", \"role\" : \"COMPETITOR\"}", new User(1, "Jovan", "john88@gmail.com", "Jhonny88!", ApplicationUserRole.COMPETITOR)),
                Arguments.of("{}", new User(1, "John", "john88@gmail.com", "Jhonny88!", ApplicationUserRole.ADMIN))
//                Arguments.of("{\"email\" : \"bulatn@gmail.com\", \"password\" : \"Password33*\"}", new User(1, "John", "bulatn@gmail.com", "Password33*", ApplicationUserRole.ADMIN)),
//                Arguments.of("{\"email\" : \"bulatn@gmail.com\", \"password\" : \"Password33*\", \"name\" : \"Jovan\", \"role\" : \"COMPETITOR\"}"
//                        , new User(1, "Jovan", "bulatn@gmail.com", "Password33*", ApplicationUserRole.COMPETITOR)),
//                Arguments.of("{\"name\" : \"Boban\", \"password\" : \"NewPass77!\"}", new User(1, "Boban", "john88@gmail.com", "NewPass77!", ApplicationUserRole.ADMIN))
        );
    }
}
