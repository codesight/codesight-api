package com.codesight.codesight_api.domain.submission.service;

import com.codesight.codesight_api.domain.challenge.entity.Challenge;
import com.codesight.codesight_api.domain.challenge.entity.Difficulty;
import com.codesight.codesight_api.domain.challenge.repository.ChallengeRepository;
import com.codesight.codesight_api.domain.submission.entity.Submission;
import com.codesight.codesight_api.domain.submission.repository.SubmissionRepository;
import com.codesight.codesight_api.domain.user.entity.ApplicationUserRole;
import com.codesight.codesight_api.domain.user.entity.User;
import com.codesight.codesight_api.domain.user.repository.UserRepository;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.challenges.ChallengeNotFoundException;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.submissions.SubmissionNotFoundException;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.users.InvalidUserException;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.users.UserNotFoundException;
import com.codesight.codesight_api.web.dtos.submission.*;
import com.codesight.codesight_api.web.mappers.SubmissionMapperImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SubmissionServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private ChallengeRepository challengeRepository;

    @Mock
    private SubmissionRepository submissionRepository;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    Authentication auth;

    @Spy
    SubmissionMapperImpl submissionMapper;

    @InjectMocks
    private SubmissionServiceImpl submissionService;

    @Value("${code-execution.url}")
    private String JUDGE0_API_URL;
    private static final int CHALLENGE_ID = 1;
    private static final int USER_ID = 1;
    private static SubmissionParameter acceptedSubmissionParameter;
    private static SubmissionParameter notAcceptedSubmissionParameter;
    private static SubmissionResponse acceptedSubmissionResponse;
    private SubmissionResponse notAcceptedSubmissionResponse;
    private static Challenge challenge;
    private static User user;
    private static SubmissionPostDto acceptedSubmissionPostDto;
    private static Pageable pageable;
    private static Collection<Challenge> challenges;

    @BeforeAll
    public static void initBeforeAll(){
        acceptedSubmissionParameter = 
                new SubmissionParameter("public class Main {public static void main(String[] args) {System.out.print(\"CHECK\");}}", 62, null);
        notAcceptedSubmissionParameter =
                new SubmissionParameter("public class Main {public static void main(String[] args) {System.out.print(\"CHECK\");}}", 62, "WRONG");
        acceptedSubmissionResponse = new SubmissionResponse.Builder()
                .withOutput("CHECK")
                .withStatus(new Status(3, "Accepted"))
                .build();
        challenge = new Challenge(CHALLENGE_ID, "Insertion sort","Sort", Difficulty.EASY, 70);
        user = new User(USER_ID, "John", "john@gmail.com", "Johnson33*", ApplicationUserRole.COMPETITOR);
        acceptedSubmissionPostDto = new SubmissionPostDto(CHALLENGE_ID, USER_ID, acceptedSubmissionParameter);
        pageable = PageRequest.of(0,3);
        challenges = List.of(challenge);
    }

    @BeforeEach
    public void initBeforeEach(){
        notAcceptedSubmissionResponse = new SubmissionResponse.Builder()
                .withOutput("CHECK")
                .withStatus(new Status(4, "Wrong Answer"))
                .withWrongAnswerMessage("Your output: CHECK doesn't match the expected output: WRONG")
                .withExpectedOutput("WRONG")
                .build();
    }

    @Test
    void checkCodeShouldReturnResponseWhenAccepted(){
        when(restTemplate.postForObject(JUDGE0_API_URL, acceptedSubmissionParameter, SubmissionResponse.class)).thenReturn(acceptedSubmissionResponse);

        assertThat(submissionService.checkCode(acceptedSubmissionParameter)).usingRecursiveComparison().isEqualTo(acceptedSubmissionResponse);
    }

    @Test
    void checkCodeShouldReturnResponseWhenNotAccepted(){
        when(restTemplate.postForObject(JUDGE0_API_URL, notAcceptedSubmissionParameter, SubmissionResponse.class)).thenReturn(notAcceptedSubmissionResponse);

        assertThat(submissionService.checkCode(notAcceptedSubmissionParameter)).usingRecursiveComparison().isEqualTo(notAcceptedSubmissionResponse);
    }

    @Test
    void submitCodeWillSaveWhenStatusIsAccepted(){
        when(restTemplate.postForObject(JUDGE0_API_URL, acceptedSubmissionParameter, SubmissionResponse.class)).thenReturn(acceptedSubmissionResponse);
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        when(challengeRepository.findById(anyInt())).thenReturn(Optional.of(challenge));
        when(submissionRepository.findByChallengeAndCompetitor(any(Challenge.class), any(User.class))).thenReturn(Optional.empty());
        when(auth.getPrincipal()).thenReturn(user.getEmail());

        submissionService.submitCode(acceptedSubmissionPostDto, auth);

        verify(submissionRepository, times(1)).save(any(Submission.class));
    }

    @Test
    void submitCodeWillSaveAndOverwriteWhenStatusIsAccepted(){
        when(restTemplate.postForObject(JUDGE0_API_URL, acceptedSubmissionParameter, SubmissionResponse.class)).thenReturn(acceptedSubmissionResponse);
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        when(challengeRepository.findById(anyInt())).thenReturn(Optional.of(challenge));
        when(submissionRepository.findByChallengeAndCompetitor(any(Challenge.class), any(User.class))).thenReturn(Optional.of(new Submission.Builder().build()));
        when(auth.getPrincipal()).thenReturn(user.getEmail());

        submissionService.submitCode(acceptedSubmissionPostDto, auth);

        verify(submissionRepository, times(1)).save(any(Submission.class));
    }

    @Test
    void submitCodeShouldNotSaveWhenStatusIsNotAccepted(){
        SubmissionPostDto submissionPostDto = new SubmissionPostDto(1, 1, notAcceptedSubmissionParameter);

        when(restTemplate.postForObject(JUDGE0_API_URL, notAcceptedSubmissionParameter, SubmissionResponse.class)).thenReturn(notAcceptedSubmissionResponse);

        submissionService.submitCode(submissionPostDto, auth);

        verify(submissionRepository, times(0)).save(any(Submission.class));
    }

    @Test
    void submitCodeWillThrowWhenChallengeDoesNotExist(){
        when(restTemplate.postForObject(JUDGE0_API_URL, acceptedSubmissionParameter, SubmissionResponse.class)).thenReturn(acceptedSubmissionResponse);
        when(challengeRepository.findById(anyInt())).thenReturn(Optional.empty());

        Assertions.assertThrows(ChallengeNotFoundException.class, ()->{
           submissionService.submitCode(acceptedSubmissionPostDto, auth);
        });
    }

    @Test
    void submitCodeWillThrowWhenUserDoesNotExist(){
        when(restTemplate.postForObject(JUDGE0_API_URL, acceptedSubmissionParameter, SubmissionResponse.class)).thenReturn(acceptedSubmissionResponse);
        when(userRepository.findById(anyInt())).thenReturn(Optional.empty());
        when(challengeRepository.findById(anyInt())).thenReturn(Optional.of(challenge));

        Assertions.assertThrows(UserNotFoundException.class, ()->{
            submissionService.submitCode(acceptedSubmissionPostDto, auth);
        });
    }

    @Test
    void submitCodeWillThrowWhenInvalidUser(){
        when(restTemplate.postForObject(JUDGE0_API_URL, acceptedSubmissionParameter, SubmissionResponse.class)).thenReturn(acceptedSubmissionResponse);
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        when(challengeRepository.findById(anyInt())).thenReturn(Optional.of(challenge));
        when(auth.getPrincipal()).thenReturn("wrong email");

        Assertions.assertThrows(InvalidUserException.class, ()->{
            submissionService.submitCode(acceptedSubmissionPostDto, auth);
        });
    }

    @Test
    void getAllShouldReturnAllSubmissions(){
        when(submissionRepository.findAll(pageable)).thenReturn(Page.empty());

        submissionService.get(pageable);

        verify(submissionRepository, times(1)).findAll(pageable);
    }

    @Test
    void getAllByCompetitorIdShouldReturnAllSubmissionsByCompetitorId(){
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        when(auth.getPrincipal()).thenReturn(user.getEmail());
        when(auth.getAuthorities()).thenReturn(Collections.EMPTY_SET);
        when(submissionRepository.findAllByCompetitor(user, pageable)).thenReturn(Page.empty());

        submissionService.getByCompetitorId(user.getId(), pageable, auth);

        verify(submissionRepository, times(1)).findAllByCompetitor(user, pageable);
    }

    @Test
    void getAllByCompetitorIdWillThrowWhenUserDoesntExist(){
        when(userRepository.findById(anyInt())).thenReturn(Optional.empty());

        Assertions.assertThrows(UserNotFoundException.class, ()->{
            submissionService.getByCompetitorId(user.getId(), pageable, auth);
        });
    }

    @Test
    void getAllByCompetitorIdWillThrowWhenInvalidUser(){
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        when(auth.getPrincipal()).thenReturn("wrong email");
        when(auth.getAuthorities()).thenReturn(Collections.EMPTY_SET);

        Assertions.assertThrows(InvalidUserException.class, ()->{
            submissionService.getByCompetitorId(user.getId(), pageable, auth);
        });
    }

    @Test
    void getAllByAuthorIdShouldReturnAllSubmissionsByAuthorId(){
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        when(challengeRepository.findAllByAuthor(user)).thenReturn(challenges);
        when(auth.getPrincipal()).thenReturn(user.getEmail());
        when(auth.getAuthorities()).thenReturn(Collections.EMPTY_SET);
        when(submissionRepository.findAllByChallengeIn(challenges, pageable)).thenReturn(Page.empty());

        submissionService.getByAuthorId(user.getId(), pageable, auth);

        verify(submissionRepository, times(1)).findAllByChallengeIn(challenges, pageable);
    }

    @Test
    void getAllByAuthorIdWillThrowWhenUserDoesntExist(){
        when(userRepository.findById(anyInt())).thenReturn(Optional.empty());

        Assertions.assertThrows(UserNotFoundException.class, ()->{
            submissionService.getByAuthorId(user.getId(), pageable, auth);
        });
    }

    @Test
    void getAllByAuthorIdWillThrowWhenInvalidUser(){
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        when(challengeRepository.findAllByAuthor(user)).thenReturn(challenges);
        when(auth.getPrincipal()).thenReturn("wrong email");
        when(auth.getAuthorities()).thenReturn(Collections.EMPTY_SET);

        Assertions.assertThrows(InvalidUserException.class, ()->{
            submissionService.getByAuthorId(user.getId(), pageable, auth);
        });
    }

    @Test
    void getByCompetitorAndChallengeShouldReturnSubmissionByCompetitorAndChallenge(){
        Submission submission = new Submission.Builder()
                                    .withId(1)
                                    .withSourceCode("public class Main {public static void main(String[] args) {System.out.print(\"CHECK\");}}")
                                    .withResult("CHECK")
                                    .withLanguageId(62)
                                    .withScore(90)
                                    .withUser(user)
                                    .withChallenge(challenge)
                                    .build();

        when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        when(auth.getPrincipal()).thenReturn(user.getEmail());
        when(auth.getAuthorities()).thenReturn(Collections.EMPTY_SET);
        when(challengeRepository.findById(anyInt())).thenReturn(Optional.of(challenge));
        when(submissionRepository.findByChallengeAndCompetitor(challenge,user)).thenReturn(Optional.of(submission));

        SubmissionGetDto fetchedSubmissionGetDto = submissionService.getByCompetitorAndChallenge(user.getId(), challenge.getId(), auth);

        assertThat(fetchedSubmissionGetDto).usingRecursiveComparison().isEqualTo(submissionMapper.submissionToSubmissionGetDto(submission));
    }

    @Test
    void getByCompetitorAndChallengeWillThrowUserNotFoundException(){
        when(userRepository.findById(anyInt())).thenReturn(Optional.empty());

        Assertions.assertThrows(UserNotFoundException.class, ()->{
            submissionService.getByCompetitorAndChallenge(user.getId(), challenge.getId(), auth);
        });
    }

    @Test
    void getByCompetitorAndChallengeWillThrowInvalidUserException(){
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        when(auth.getPrincipal()).thenReturn("wrong email");
        when(auth.getAuthorities()).thenReturn(Collections.EMPTY_SET);

        Assertions.assertThrows(InvalidUserException.class, ()->{
            submissionService.getByCompetitorAndChallenge(user.getId(), challenge.getId(), auth);
        });
    }

    @Test
    void getByCompetitorAndChallengeWillThrowChallengeNotFoundException(){
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        when(auth.getPrincipal()).thenReturn(user.getEmail());
        when(auth.getAuthorities()).thenReturn(Collections.EMPTY_SET);
        when(challengeRepository.findById(anyInt())).thenReturn(Optional.empty());

        Assertions.assertThrows(ChallengeNotFoundException.class, ()->{
            submissionService.getByCompetitorAndChallenge(user.getId(), challenge.getId(), auth);
        });
    }

    @Test
    void getByCompetitorAndChallengeWillThrowSubmissionNotFoundException(){
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        when(auth.getPrincipal()).thenReturn(user.getEmail());
        when(auth.getAuthorities()).thenReturn(Collections.EMPTY_SET);
        when(challengeRepository.findById(anyInt())).thenReturn(Optional.of(challenge));
        when(submissionRepository.findByChallengeAndCompetitor(challenge,user)).thenReturn(Optional.empty());

        Assertions.assertThrows(SubmissionNotFoundException.class, ()->{
            submissionService.getByCompetitorAndChallenge(user.getId(), challenge.getId(), auth);
        });
    }
}
