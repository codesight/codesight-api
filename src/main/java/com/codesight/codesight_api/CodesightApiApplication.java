package com.codesight.codesight_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodesightApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodesightApiApplication.class, args);
    }
}
