package com.codesight.codesight_api.web.mappers;

import com.codesight.codesight_api.domain.submission.entity.Submission;
import com.codesight.codesight_api.web.dtos.submission.SubmissionGetDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface SubmissionMapper {
    @Mapping(target = "user", source = "competitor")
    SubmissionGetDto submissionToSubmissionGetDto(Submission submission);
}
