package com.codesight.codesight_api.web.controllers;

import com.codesight.codesight_api.web.dtos.submission.SubmissionGetDto;
import com.codesight.codesight_api.web.dtos.submission.SubmissionResponse;
import com.codesight.codesight_api.web.dtos.submission.SubmissionParameter;
import com.codesight.codesight_api.domain.submission.service.SubmissionService;
import com.codesight.codesight_api.domain.submission.service.SubmissionServiceImpl;
import com.codesight.codesight_api.web.dtos.submission.SubmissionPostDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(value = "Submission-Controller")
@Controller
@RequestMapping("/api/submissions")
public class SubmissionController {

    private final SubmissionService submissionService;

    @Autowired
    public SubmissionController(SubmissionService submissionService) {
        this.submissionService = submissionService;
    }

    @ApiOperation(value = "Get list of submissions")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK"),
            @ApiResponse(code = 400, message = "Bad request|BAD_REQUEST")
    })
    @GetMapping(produces = "application/json")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Page<SubmissionGetDto>> get(Pageable pageable){
        return ResponseEntity.ok(submissionService.get(pageable));
    }

    @ApiOperation(value = "Get list of submissions by competitor id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK"),
            @ApiResponse(code = 400, message = "Bad request|BAD_REQUEST"),
            @ApiResponse(code = 404, message = "Competitor not found|NOT_FOUND"),
    })
    @GetMapping(value = "/competitor/{id}", produces = "application/json")
    public ResponseEntity<Page<SubmissionGetDto>> getByCompetitorId(@PathVariable("id") Integer userId, Pageable pageable, Authentication auth){
        return ResponseEntity.ok(submissionService.getByCompetitorId(userId, pageable, auth));
    }

    @ApiOperation(value = "Get list of submissions by author id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK"),
            @ApiResponse(code = 400, message = "Bad request|BAD_REQUEST"),
            @ApiResponse(code = 404, message = "Author not found|NOT_FOUND"),
    })
    @GetMapping(value = "/author/{id}", produces = "application/json")
    public ResponseEntity<Page<SubmissionGetDto>> getByAuthorId(@PathVariable("id") Integer userId, Pageable pageable, Authentication auth){
        return ResponseEntity.ok(submissionService.getByAuthorId(userId, pageable, auth));
    }

    @ApiOperation(value = "Get submission by competitor id and challenge id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK"),
            @ApiResponse(code = 400, message = "Bad request|BAD_REQUEST"),
            @ApiResponse(code = 404, message = "Not found|NOT_FOUND"),
    })
    @GetMapping(value = "/{competitorId},{challengeId}", produces = "application/json")
    public ResponseEntity<SubmissionGetDto> get(@PathVariable Integer competitorId, @PathVariable Integer challengeId, Authentication auth){
        return ResponseEntity.ok(submissionService.getByCompetitorAndChallenge(competitorId, challengeId, auth));
    }

    @ApiOperation(value = "Check if source code is accepted")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Code has status ACCEPTED|OK"),
            @ApiResponse(code = 400, message = "Bad request|BAD_REQUEST")
    })
    @PostMapping(produces = "application/json", path = "/check")
    public ResponseEntity<SubmissionResponse> checkCode(@RequestBody @Valid SubmissionParameter submissionParameter){
        SubmissionResponse response = submissionService.checkCode(submissionParameter);
        return response.getStatus().getId() == SubmissionServiceImpl.ACCEPTED_STATUS_ID ? ResponseEntity.ok(response) : new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ApiOperation(value = "Check the source code and submit if it has status ACCEPTED")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Code has been submitted|OK"),
            @ApiResponse(code = 400, message = "Bad request|BAD_REQUEST")
    })
    @PostMapping(path = "/submit", produces = "application/json")
    public ResponseEntity<SubmissionResponse> submitCode(@RequestBody @Valid SubmissionPostDto submissionPostDto, Authentication auth){
        SubmissionResponse response = submissionService.submitCode(submissionPostDto, auth);
        return response.getStatus().getId() == SubmissionServiceImpl.ACCEPTED_STATUS_ID ? ResponseEntity.ok(response) : new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
