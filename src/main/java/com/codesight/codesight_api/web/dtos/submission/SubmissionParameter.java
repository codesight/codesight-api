package com.codesight.codesight_api.web.dtos.submission;

import com.codesight.codesight_api.infrastructure.custom_annotations.SupportedProgrammingLanguage;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class SubmissionParameter {

    @NotBlank( message = "Source code field can't be null or blank")
    @JsonProperty("source_code")
    private String sourceCode;
    @JsonProperty("language_id")
    @NotNull(message = "Language id field can't be null")
    @SupportedProgrammingLanguage
    private Integer languageId;
    @JsonProperty("expected_output")
    private String expectedOutput;


    public SubmissionParameter(String sourceCode, Integer languageId, String expectedOutput) {
        this.sourceCode = sourceCode;
        this.languageId = languageId;
        this.expectedOutput = expectedOutput;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public String getExpectedOutput() {
        return expectedOutput;
    }

    public void setExpectedOutput(String expectedOutput) {
        this.expectedOutput = expectedOutput;
    }
}
