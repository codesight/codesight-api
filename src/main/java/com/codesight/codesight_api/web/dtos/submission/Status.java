package com.codesight.codesight_api.web.dtos.submission;

public class Status {

    private int id;
    private String description;

    public int getId() {
        return id;
    }

    public Status(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public Status() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
