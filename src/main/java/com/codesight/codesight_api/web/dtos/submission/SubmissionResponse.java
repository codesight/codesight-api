package com.codesight.codesight_api.web.dtos.submission;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubmissionResponse {

    @JsonProperty("stdout")
    private String output;
    @JsonProperty("expected_output")
    private String expectedOutput;
    private Status status;
    @JsonProperty("compile_output")
    private String compileOutput;
    @JsonProperty("stderr")
    private String standardError;
    private String message;
    private String wrongAnswerMessage;

    private SubmissionResponse(){}

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getExpectedOutput() {
        return expectedOutput;
    }

    public void setExpectedOutput(String expectedOutput) {
        this.expectedOutput = expectedOutput;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getCompileOutput() {
        return compileOutput;
    }

    public void setCompileOutput(String compileOutput) {
        this.compileOutput = compileOutput;
    }

    public String getStandardError() {
        return standardError;
    }

    public void setStandardError(String standardError) {
        this.standardError = standardError;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getWrongAnswerMessage() {
        return wrongAnswerMessage;
    }

    public void setWrongAnswerMessage(String wrongAnswerMessage) {
        this.wrongAnswerMessage = wrongAnswerMessage;
    }

    public static class Builder{

        private String output;
        private String expectedOutput;
        private Status status;
        private String compileOutput;
        private String standardError;
        private String message;
        private String wrongAnswerMessage;

        public Builder withOutput(String output){
            this.output = output;

            return this;
        }

        public Builder withExpectedOutput(String expectedOutput){
            this.expectedOutput = expectedOutput;

            return this;
        }

        public Builder withStatus(Status status){
            this.status = status;

            return this;
        }

        public Builder withCompileOutput(String compileOutput){
            this.compileOutput = compileOutput;

            return this;
        }

        public Builder withStandardError(String standardError){
            this.standardError = standardError;

            return this;
        }

        public Builder withMessage(String message){
            this.message = message;

            return this;
        }

        public Builder withWrongAnswerMessage(String wrongAnswerMessage){
            this.wrongAnswerMessage = wrongAnswerMessage;

            return this;
        }

        public SubmissionResponse build(){
            SubmissionResponse response = new SubmissionResponse();
            response.output = this.output;
            response.expectedOutput = this.expectedOutput;
            response.status = this.status;
            response.compileOutput = this.compileOutput;
            response.standardError = this.standardError;
            response.message = this.message;
            response.wrongAnswerMessage = this.wrongAnswerMessage;

            return response;
        }
    }
}
