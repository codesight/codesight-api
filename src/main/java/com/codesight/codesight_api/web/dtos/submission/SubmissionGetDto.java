package com.codesight.codesight_api.web.dtos.submission;

import com.codesight.codesight_api.web.dtos.challenge.ChallengeGetDto;
import com.codesight.codesight_api.web.dtos.user.UserGetDto;

public class SubmissionGetDto {

    private int id;
    private String sourceCode;
    private String result;
    private Integer languageId;
    private int score;
    private UserGetDto user;
    private ChallengeGetDto challenge;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public UserGetDto getUser() {
        return user;
    }

    public void setUser(UserGetDto user) {
        this.user = user;
    }

    public ChallengeGetDto getChallenge() {
        return challenge;
    }

    public void setChallenge(ChallengeGetDto challenge) {
        this.challenge = challenge;
    }
}
