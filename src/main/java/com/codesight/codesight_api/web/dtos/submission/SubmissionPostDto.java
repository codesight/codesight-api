package com.codesight.codesight_api.web.dtos.submission;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class SubmissionPostDto {

    @NotNull(message = "Challenge ID field is required")
    @JsonProperty("challenge_id")
    private Integer challengeId;

    @NotNull(message = "User ID field is required")
    @JsonProperty("user_id")
    private Integer userId;

    @NotNull(message = "Submission parameter field is required")
    @JsonProperty("submission_parameter")
    @Valid
    private SubmissionParameter submissionParameter;

    public SubmissionPostDto(Integer challengeId, Integer userId, SubmissionParameter submissionParameter) {
        this.challengeId = challengeId;
        this.userId = userId;
        this.submissionParameter = submissionParameter;
    }

    public SubmissionPostDto() {}

    public Integer getChallengeId() {
        return challengeId;
    }

    public void setChallengeId(Integer challengeId) {
        this.challengeId = challengeId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public SubmissionParameter getSubmissionParameter() {
        return submissionParameter;
    }

    public void setSubmissionParameter(SubmissionParameter submissionParameter) {
        this.submissionParameter = submissionParameter;
    }
}
