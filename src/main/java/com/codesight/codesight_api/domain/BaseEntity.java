package com.codesight.codesight_api.domain;

import javax.persistence.*;

@MappedSuperclass
public class BaseEntity {

    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
