package com.codesight.codesight_api.domain.user.entity;

public enum ApplicationUserRole {
    ADMIN,
    COMPETITOR
}
