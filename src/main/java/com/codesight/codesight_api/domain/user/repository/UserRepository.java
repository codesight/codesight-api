package com.codesight.codesight_api.domain.user.repository;

import com.codesight.codesight_api.domain.user.entity.ApplicationUserRole;
import com.codesight.codesight_api.domain.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    boolean existsByEmail(String email);
    Optional<User> findByEmail(String email);
    Set<User> findByRole(ApplicationUserRole role);
}
