package com.codesight.codesight_api.domain.submission.repository;

import com.codesight.codesight_api.domain.challenge.entity.Challenge;
import com.codesight.codesight_api.domain.submission.entity.Submission;
import com.codesight.codesight_api.domain.user.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface SubmissionRepository extends JpaRepository<Submission, Integer> {

    Optional<Submission> findByChallengeAndCompetitor(Challenge challenge, User competitor);
    Page<Submission> findAllByCompetitor(User competitor, Pageable pageable);
    Page<Submission> findAllByChallengeIn(Collection<Challenge> challenges, Pageable pageable);
}
