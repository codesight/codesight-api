package com.codesight.codesight_api.domain.submission.entity;

import com.codesight.codesight_api.domain.challenge.entity.Challenge;
import com.codesight.codesight_api.domain.user.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity(name = "submissions")
public class Submission {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String sourceCode;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String result;

    @Column(nullable = false, name = "language_number")
    private Integer languageId;

    private int score;

    @JsonIgnore
    @ManyToOne
    private User competitor;

    @ManyToOne
    private Challenge challenge;

    protected Submission() { }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public User getCompetitor() {
        return competitor;
    }

    public void setCompetitor(User user) {
        this.competitor = user;
    }

    public Challenge getChallenge() {
        return challenge;
    }

    public void setChallenge(Challenge challenge) {
        this.challenge = challenge;
    }

    public static class Builder{

        private Integer id;
        private String sourceCode;
        private String result;
        private Integer languageId;
        private int score;
        private User user;
        private Challenge challenge;

        public Builder withId(Integer id){
            this.id = id;

            return this;
        }

        public Builder withSourceCode(String sourceCode){
            this.sourceCode = sourceCode;

            return this;
        }

        public Builder withResult(String result){
            this.result = result;

            return this;
        }

        public Builder withLanguageId(Integer languageId){
            this.languageId = languageId;

            return this;
        }

        public Builder withScore(int score){
            this.score = score;

            return this;
        }

        public Builder withUser(User user){
            this.user = user;

            return this;
        }

        public Builder withChallenge(Challenge challenge){
            this.challenge = challenge;

            return this;
        }

        public Submission build(){
            Submission submission = new Submission();
            submission.id = this.id;
            submission.sourceCode = this.sourceCode;
            submission.result = this.result;
            submission.languageId = this.languageId;
            submission.score = this.score;
            submission.competitor = this.user;
            submission.challenge = this.challenge;

            return submission;
        }
    }
}
