package com.codesight.codesight_api.domain.submission.service;

import com.codesight.codesight_api.web.dtos.submission.SubmissionGetDto;
import com.codesight.codesight_api.web.dtos.submission.SubmissionResponse;
import com.codesight.codesight_api.web.dtos.submission.SubmissionParameter;
import com.codesight.codesight_api.web.dtos.submission.SubmissionPostDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;

public interface SubmissionService {

    SubmissionResponse checkCode(SubmissionParameter submissionParameter);
    SubmissionResponse submitCode(SubmissionPostDto submissionPostDto, Authentication auth);
    Page<SubmissionGetDto> get(Pageable pageable);
    Page<SubmissionGetDto> getByCompetitorId(Integer userId, Pageable pageable, Authentication auth);
    Page<SubmissionGetDto> getByAuthorId(Integer userId, Pageable pageable, Authentication auth);
    SubmissionGetDto getByCompetitorAndChallenge(Integer competitorId, Integer challengeId, Authentication auth);
}
