package com.codesight.codesight_api.domain.submission.service;

import com.codesight.codesight_api.domain.challenge.entity.Challenge;
import com.codesight.codesight_api.domain.challenge.repository.ChallengeRepository;
import com.codesight.codesight_api.domain.submission.entity.Submission;
import com.codesight.codesight_api.domain.user.entity.ApplicationUserRole;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.submissions.SubmissionNotFoundException;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.users.InvalidUserException;
import com.codesight.codesight_api.web.dtos.submission.SubmissionGetDto;
import com.codesight.codesight_api.web.dtos.submission.SubmissionResponse;
import com.codesight.codesight_api.web.dtos.submission.SubmissionParameter;
import com.codesight.codesight_api.domain.submission.repository.SubmissionRepository;
import com.codesight.codesight_api.domain.user.entity.User;
import com.codesight.codesight_api.domain.user.repository.UserRepository;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.challenges.ChallengeNotFoundException;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.users.UserNotFoundException;
import com.codesight.codesight_api.web.dtos.submission.SubmissionPostDto;
import com.codesight.codesight_api.web.mappers.SubmissionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;
import java.util.Optional;

@Service
public class SubmissionServiceImpl implements SubmissionService {

    public static final int ACCEPTED_STATUS_ID = 3;
    private static final int WRONG_ANSWER_STATUS_ID = 4;
    @Value("${code-execution.url}")
    private String JUDGE0_API_URL;

    private final RestTemplate restTemplate;
    private final UserRepository userRepository;
    private final ChallengeRepository challengeRepository;
    private final SubmissionRepository submissionRepository;
    private final SubmissionMapper submissionMapper;

    @Autowired
    public SubmissionServiceImpl(RestTemplate restTemplate, UserRepository userRepository, ChallengeRepository challengeRepository,
                                 SubmissionRepository submissionRepository, SubmissionMapper submissionMapper) {
        this.restTemplate = restTemplate;
        this.userRepository = userRepository;
        this.challengeRepository = challengeRepository;
        this.submissionRepository = submissionRepository;
        this.submissionMapper = submissionMapper;
    }

    @Override
    public Page<SubmissionGetDto> get(Pageable pageable) {
        return submissionRepository.findAll(pageable).map(submissionMapper::submissionToSubmissionGetDto);
    }

    @Override
    public Page<SubmissionGetDto> getByCompetitorId(Integer competitorId, Pageable pageable, Authentication auth) {
        User competitor = userRepository.findById(competitorId).orElseThrow(() -> new UserNotFoundException(competitorId));
        if(!isAdmin(auth) && !isLoggedInUser(auth, competitor.getEmail())) throw new InvalidUserException();
        return submissionRepository.findAllByCompetitor(competitor, pageable).map(submissionMapper::submissionToSubmissionGetDto);
    }

    @Override
    public Page<SubmissionGetDto> getByAuthorId(Integer authorId, Pageable pageable, Authentication auth) {
        User author = userRepository.findById(authorId).orElseThrow(() -> new UserNotFoundException(authorId));
        Collection<Challenge> challenges = challengeRepository.findAllByAuthor(author);
        if(!isAdmin(auth) && !isLoggedInUser(auth, author.getEmail())) throw new InvalidUserException();

        return submissionRepository.findAllByChallengeIn(challenges, pageable).map(submissionMapper::submissionToSubmissionGetDto);
    }

    @Override
    public SubmissionGetDto getByCompetitorAndChallenge(Integer competitorId, Integer challengeId, Authentication auth) {
        User competitor = userRepository.findById(competitorId).orElseThrow(() -> new UserNotFoundException(competitorId));

        if(!isAdmin(auth) && !isLoggedInUser(auth, competitor.getEmail())) throw new InvalidUserException();

        Challenge challenge = challengeRepository.findById(challengeId).orElseThrow(
                () -> new ChallengeNotFoundException(String.format("Challenge with id '%d' not found in DB", challengeId)));

        Submission submission = submissionRepository.findByChallengeAndCompetitor(challenge, competitor).orElseThrow(
                () -> new SubmissionNotFoundException(competitorId, challengeId));

        return submissionMapper.submissionToSubmissionGetDto(submission);
    }

    @Override
    public SubmissionResponse checkCode(SubmissionParameter submissionParameter) {
        return getResponse(submissionParameter);
    }

    @Override
    public SubmissionResponse submitCode(SubmissionPostDto submissionPostDto, Authentication auth) {
        SubmissionResponse response = getResponse(submissionPostDto.getSubmissionParameter());
        if (response.getStatus().getId() == ACCEPTED_STATUS_ID) saveSubmission(response, submissionPostDto, auth);
        return response;
    }

    private void saveSubmission(SubmissionResponse response, SubmissionPostDto submissionPostDto, Authentication auth) {
        Challenge challenge = challengeRepository.findById(submissionPostDto.getChallengeId()).orElseThrow(
                () -> new ChallengeNotFoundException(String.format("Challenge with id '%d' not found in DB", submissionPostDto.getChallengeId())));
        User user = userRepository.findById(submissionPostDto.getUserId()).orElseThrow(() -> new UserNotFoundException(submissionPostDto.getUserId()));

        if(!isLoggedInUser(auth, user.getEmail())) throw new InvalidUserException();

        submissionRepository.save(getSubmission(response, submissionPostDto.getSubmissionParameter(), challenge, user));
    }

    private Submission getSubmission(SubmissionResponse response, SubmissionParameter submissionParameter, Challenge challenge, User user) {
        Optional<Submission> existingSubmission = submissionRepository.findByChallengeAndCompetitor(challenge, user);

        return existingSubmission.isEmpty() ? getSubmissionWithoutId(response, submissionParameter, challenge, user)
                : getSubmissionWithId(response, submissionParameter, challenge, user, existingSubmission.get().getId());
    }

    private Submission getSubmissionWithoutId(SubmissionResponse response, SubmissionParameter submissionParameter, Challenge challenge, User user) {
        return new Submission.Builder()
                .withSourceCode(submissionParameter.getSourceCode())
                .withResult(response.getOutput())
                .withLanguageId(submissionParameter.getLanguageId())
                .withScore(challenge.getPoints())
                .withUser(user)
                .withChallenge(challenge)
                .build();
    }

    private Submission getSubmissionWithId(SubmissionResponse response, SubmissionParameter submissionParameter, Challenge challenge, User user, Integer id) {
        Submission submission = getSubmissionWithoutId(response, submissionParameter, challenge, user);
        submission.setId(id);
        return submission;
    }

    private SubmissionResponse getResponse(SubmissionParameter submissionParameter) {
        SubmissionResponse response = Optional.ofNullable(restTemplate.postForObject(JUDGE0_API_URL, submissionParameter, SubmissionResponse.class)).orElseThrow();

        response.setExpectedOutput(submissionParameter.getExpectedOutput());
        if (response.getStatus().getId() == WRONG_ANSWER_STATUS_ID)
            response.setWrongAnswerMessage("Your output: " + response.getOutput() + " doesn't match the expected output: " + response.getExpectedOutput());
        return response;
    }

    private boolean isAdmin(Authentication auth){
        return auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals(ApplicationUserRole.ADMIN.name()));
    }

    private boolean isLoggedInUser(Authentication auth, String email){
        return email.equals(auth.getPrincipal());
    }
}
