package com.codesight.codesight_api.domain.challenge.entity;

import com.codesight.codesight_api.domain.BaseEntity;
import com.codesight.codesight_api.domain.submission.entity.Submission;
import com.codesight.codesight_api.domain.user.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;

@Entity
public class Challenge extends BaseEntity {

    @NotNull(message = "Challenge name can't be null")
    @Size(min = 2, max = 100, message = "Challenge name length should be between 2 and 100 characters")
    private String name;

    @NotEmpty(message = "Description can't be null or empty")
    private String description;

    @NotNull(message = "Difficulty can't be null")
    @Enumerated(EnumType.STRING)
    private Difficulty difficulty;

    @Min(value = 1, message = "Points should not be less than 1")
    @Max(value = 300, message = "Points should not be greater than 300")
    private int points;

    @JsonIgnore
    @ManyToOne
    private User author;

    @OneToMany(mappedBy = "challenge", cascade = CascadeType.ALL)
    private List<Submission> submissions;

    public Challenge(String name, String description, Difficulty difficulty, int points) {
        this.name = name;
        this.description = description;
        this.difficulty = difficulty;
        this.points = points;
    }

    public Challenge(int id, String name, String description, Difficulty difficulty, int points) {
        super.setId(id);
        this.name = name;
        this.description = description;
        this.difficulty = difficulty;
        this.points = points;
    }
    public Challenge() {
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public int getPoints() {
        return points;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User user) {
        this.author = user;
    }

    public List<Submission> getSubmissions() {
        return submissions;
    }

    public void setSubmissions(List<Submission> submissions) {
        this.submissions = submissions;
    }
}
