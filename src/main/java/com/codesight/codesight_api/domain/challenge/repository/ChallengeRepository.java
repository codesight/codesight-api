package com.codesight.codesight_api.domain.challenge.repository;

import com.codesight.codesight_api.domain.challenge.entity.Challenge;
import com.codesight.codesight_api.domain.user.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Collection;

@Repository
public interface ChallengeRepository extends JpaRepository<Challenge, Integer> {

    Collection<Challenge> findAllByAuthor(User author);

    Page<Challenge> findAllByAuthorId(Pageable pageable, int authorId);
}
