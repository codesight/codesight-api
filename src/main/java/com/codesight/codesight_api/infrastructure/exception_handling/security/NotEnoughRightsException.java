package com.codesight.codesight_api.infrastructure.exception_handling.security;

public class NotEnoughRightsException extends RuntimeException {

    public NotEnoughRightsException() {
        super("You don't have enough rights!");
    }
}
