package com.codesight.codesight_api.infrastructure.exception_handling.exceptions.shared;

public class IncorrectJsonMergePatchProcessingException extends RuntimeException{

    public IncorrectJsonMergePatchProcessingException(String message) {
        super(message);
    }
}
