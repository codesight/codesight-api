package com.codesight.codesight_api.infrastructure.exception_handling.exceptions.users;

public class UserNotFoundException extends RuntimeException{

    public UserNotFoundException(int id){
        super("User with id " + id + " does not exist.");
    }

    public UserNotFoundException(String email){
        super("User with email " + email + " does not exist.");
    }
}
