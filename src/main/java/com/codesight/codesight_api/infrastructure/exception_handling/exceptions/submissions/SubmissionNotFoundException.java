package com.codesight.codesight_api.infrastructure.exception_handling.exceptions.submissions;

public class SubmissionNotFoundException extends RuntimeException{

    public SubmissionNotFoundException(int competitorId, int challengeId){
        super(String.format("Submission with competitor id %d and challenge id %d not found in DB", competitorId, challengeId));
    }
}
