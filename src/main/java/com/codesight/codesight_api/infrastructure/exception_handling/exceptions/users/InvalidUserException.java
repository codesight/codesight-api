package com.codesight.codesight_api.infrastructure.exception_handling.exceptions.users;

public class InvalidUserException extends RuntimeException {

    public InvalidUserException() {
        super("You can only use this feature on your own account");
    }
}