package com.codesight.codesight_api.infrastructure.exception_handling;

import com.codesight.codesight_api.domain.user.entity.ApplicationUserRole;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.challenges.ChallengeNotFoundException;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.challenges.InvalidPointsRangeException;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.shared.IdCannotBeChangedException;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.shared.IncorrectJsonMergePatchProcessingException;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.submissions.SubmissionNotFoundException;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.users.InvalidUserException;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.users.UserAlreadyExistsException;
import com.codesight.codesight_api.infrastructure.exception_handling.exceptions.users.UserNotFoundException;
import com.codesight.codesight_api.infrastructure.exception_handling.security.NotEnoughRightsException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException ex, HttpServletRequest httpRequest){
        ApiError body = new ApiError(OffsetDateTime.now(), NOT_FOUND.value(), ex.getMessage(), httpRequest.getRequestURI());
        return new ResponseEntity<>(body, NOT_FOUND);
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    public ResponseEntity<Object> handleUserAlreadyExistsException(UserAlreadyExistsException ex, HttpServletRequest httpRequest){
        ApiError body = new ApiError(OffsetDateTime.now(), BAD_REQUEST.value(), ex.getMessage(), httpRequest.getRequestURI());
        return new ResponseEntity<>(body, BAD_REQUEST);
    }

    @ExceptionHandler(IncorrectJsonMergePatchProcessingException.class)
    public ResponseEntity<Object> handleJsonMergeProcessingException(IncorrectJsonMergePatchProcessingException ex, HttpServletRequest httpRequest){
        ApiError body = new ApiError(OffsetDateTime.now(), BAD_REQUEST.value(), ex.getMessage(), httpRequest.getRequestURI());
        return new ResponseEntity<>(body, BAD_REQUEST);
    }

    @ExceptionHandler(IdCannotBeChangedException.class)
    public ResponseEntity<Object> handleIdCannotBeChangedException(IdCannotBeChangedException ex, HttpServletRequest httpRequest){
        ApiError body = new ApiError(OffsetDateTime.now(), BAD_REQUEST.value(), ex.getMessage(), httpRequest.getRequestURI());
        return new ResponseEntity<>(body, BAD_REQUEST);
    }

    @ExceptionHandler(PropertyReferenceException.class)
    public ResponseEntity<Object> handlePropertyReferenceException(PropertyReferenceException ex, HttpServletRequest httpRequest){
        ApiError body = new ApiError(OffsetDateTime.now(), BAD_REQUEST.value(), ex.getMessage(), httpRequest.getRequestURI());
        return new ResponseEntity<>(body, BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> errors = new ArrayList<>();

        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        ApiError apiError = new ApiError(OffsetDateTime.now(), BAD_REQUEST.value(), errors,
                ((ServletWebRequest) request).getRequest().getRequestURI() );

        return new ResponseEntity<>(apiError, BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object>  handleConstraintViolationException(ConstraintViolationException ex, HttpServletRequest httpServletRequest) {
        List<String> errors = new ArrayList<>();
        ex.getConstraintViolations().forEach(constraintViolation -> errors.add(constraintViolation.getMessage()));
        return new ResponseEntity<>(new ApiError(OffsetDateTime.now(), BAD_REQUEST.value(),
                errors, httpServletRequest.getRequestURI()), BAD_REQUEST);
    }
    @ExceptionHandler(ChallengeNotFoundException.class)
    public ResponseEntity<Object>  handleChallengeNotFoundException(
            ChallengeNotFoundException ex, HttpServletRequest httpServletRequest) {
        ApiError body = new ApiError(OffsetDateTime.now(), BAD_REQUEST.value(),
                ex.getMessage(), httpServletRequest.getRequestURI());
        return new ResponseEntity<>(body, NOT_FOUND);
    }

    @ExceptionHandler(InvalidPointsRangeException.class)
    public ResponseEntity<Object> handleInvalidPointsRangeException(InvalidPointsRangeException ex, HttpServletRequest httpRequest){
        ApiError body = new ApiError(OffsetDateTime.now(), BAD_REQUEST.value(), ex.getMessage(), httpRequest.getRequestURI());
        return new ResponseEntity<>(body, BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Throwable cause = ex.getCause();
        String message = cause instanceof InvalidFormatException && ((InvalidFormatException) cause).getTargetType().isAssignableFrom(ApplicationUserRole.class) ? "Invalid role type" : ex.getMessage();
        ApiError body = new ApiError(OffsetDateTime.now(), BAD_REQUEST.value(), message, ((ServletWebRequest) request).getRequest().getRequestURI());
        return new ResponseEntity<>(body, BAD_REQUEST);
    }

    @ExceptionHandler(HttpClientErrorException.class)
    public ResponseEntity<Object> handleHttpClientErrorException(HttpClientErrorException ex, HttpServletRequest httpRequest){
        ApiError body = new ApiError(OffsetDateTime.now(), UNPROCESSABLE_ENTITY.value(), ex.getMessage(), httpRequest.getRequestURI());
        return new ResponseEntity<>(body, UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<Object> handleNoSuchElementException(NoSuchElementException ex, HttpServletRequest httpRequest){
        ApiError body =
                new ApiError(OffsetDateTime.now(), INTERNAL_SERVER_ERROR.value(), "There is something wrong with the response. Please check your input and try again.", httpRequest.getRequestURI());
        return new ResponseEntity<>(body, INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(InvalidUserException.class)
    public ResponseEntity<Object> handleInvalidUserException(InvalidUserException ex, HttpServletRequest httpRequest){
        ApiError body = new ApiError(OffsetDateTime.now(), FORBIDDEN.value(), ex.getMessage(), httpRequest.getRequestURI());
        return new ResponseEntity<>(body, FORBIDDEN);
    }

    @ExceptionHandler(SubmissionNotFoundException.class)
    public ResponseEntity<Object> handleSubmissionNotFoundException(SubmissionNotFoundException ex, HttpServletRequest httpRequest){
        ApiError body = new ApiError(OffsetDateTime.now(), NOT_FOUND.value(), ex.getMessage(), httpRequest.getRequestURI());
        return new ResponseEntity<>(body, NOT_FOUND);
    }

    @ExceptionHandler(NotEnoughRightsException.class)
    public ResponseEntity<Object> handleNotEnoughRightsException(NotEnoughRightsException ex, HttpServletRequest httpRequest){
        ApiError body = new ApiError(OffsetDateTime.now(), BAD_REQUEST.value(), ex.getMessage(), httpRequest.getRequestURI());
        return new ResponseEntity<>(body, BAD_REQUEST);
    }
    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public ResponseEntity<Object> handleSQLIntegrityConstraintViolationException(SQLIntegrityConstraintViolationException ex, HttpServletRequest httpRequest){
        ApiError body = new ApiError(OffsetDateTime.now(), BAD_REQUEST.value(), "Your code must have standard output", httpRequest.getRequestURI());
        return new ResponseEntity<>(body, BAD_REQUEST);
    }
}
