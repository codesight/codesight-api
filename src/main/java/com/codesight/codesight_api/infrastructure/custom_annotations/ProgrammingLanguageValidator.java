package com.codesight.codesight_api.infrastructure.custom_annotations;

import com.codesight.codesight_api.infrastructure.utils.LanguagesUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ProgrammingLanguageValidator implements ConstraintValidator<SupportedProgrammingLanguage, Integer> {
    @Override
    public void initialize(SupportedProgrammingLanguage constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(Integer languageId, ConstraintValidatorContext context) {
        return languageId != null && LanguagesUtil.isLanguageSupported(languageId);
    }
}
